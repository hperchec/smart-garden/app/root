:: --------------------------------------------------------------------------
:: CMD: Build docker images
:: @author Hervé Perchec <herve.perchec@gmail.com>
:: --------------------------------------------------------------------------

:: Disable output excepted 'echo' messages and error messages
@echo off

:: Enable UTF-8 output message (redirect output to NUL to not show message)
chcp 65001 > NUL

:: Enable color output
for /F %%a in ('echo prompt $E ^| cmd') do (
  set "ESC=%%a"
)

:: Empty line
echo.

:: Show script header
echo %ESC%[36m---------------------------------------------------------------%ESC%[0m
echo %ESC%[36mCMD: Build docker images%ESC%[0m
echo %ESC%[36mAUTHOR: Hervé Perchec ^<herve.perchec@gmail.com^>%ESC%[0m
echo %ESC%[36m---------------------------------------------------------------%ESC%[0m
echo.

:: Get directory of 'app' parent folder and move into
cd %~dp0..\
set APP_DIR=%cd%

:: --------------------------------------------------------------------------
:: Step 1: Build images
:: --------------------------------------------------------------------------

docker-compose --env-file %APP_DIR%/.docker/.env build

IF %errorlevel%==0 (
    echo.
    echo %ESC%[32mDocker images successfully built ✔%ESC%[0m
    echo.
    exit 0
) ELSE (
    echo.
    echo %ESC%[31mERROR: Unable to build docker images...%ESC%[0m
    echo.
    exit 1
)

#!/bin/bash

# --------------------------------------------------------------------------
# CMD: Build docker images
# @author Hervé Perchec <herve.perchec@gmail.com>
# --------------------------------------------------------------------------

# Get directory of 'app' parent folder and move into
SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd $SCRIPT_DIR/../
APP_DIR=$PWD

# Show header function
show_header() {
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo -e "\e[36mCMD: Build docker images\e[0m"
    echo -e "\e[36mAUTHOR: Hervé Perchec <herve.perchec@gmail.com>\e[0m"
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo
}

# --------------------------------------------------------------------------
# END INIT
# --------------------------------------------------------------------------

# Show header
show_header

# --------------------------------------------------------------------------
# Step 1: Build images
# --------------------------------------------------------------------------

docker-compose --env-file $APP_DIR/.docker/.env build

if [ $? -ne 0 ]; then
    echo
    echo -e "\e[31mERROR: Unable to build database docker images...\e[0m"
    echo
    exit 1
else
    echo
    echo -e "\e[32mDocker images successfully built ✔\e[0m"
    echo
    exit 0
fi

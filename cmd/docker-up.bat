:: --------------------------------------------------------------------------
:: CMD: Start docker containers
:: @author Hervé Perchec <herve.perchec@gmail.com>
:: --------------------------------------------------------------------------

:: Disable output excepted 'echo' messages and error messages
@echo off

:: Enable UTF-8 output message (redirect output to NUL to not show message)
chcp 65001 > NUL

:: Enable color output
for /F %%a in ('echo prompt $E ^| cmd') do (
  set "ESC=%%a"
)

:: Empty line
echo.

:: Show script header
echo %ESC%[36m---------------------------------------------------------------%ESC%[0m
echo %ESC%[36mCMD: Start docker containers%ESC%[0m
echo %ESC%[36mAUTHOR: Hervé Perchec ^<herve.perchec@gmail.com^>%ESC%[0m
echo %ESC%[36m---------------------------------------------------------------%ESC%[0m
echo.

:: Get directory of 'app' parent folder and move into
cd %~dp0..\
set APP_DIR=%cd%

:: --------------------------------------------------------------------------
:: Step 1: Prevent using of 'UI' container, due to loss of performance
:: --------------------------------------------------------------------------

echo %ESC%[33;1mWARNING:%ESC%[0m
echo.
echo %ESC%[33mWindows OS detected! Using docker container in %ESC%[1;4mdevelopment%ESC%[0m %ESC%[33mfor 'UI' subproject is not optimized.%ESC%[0m
echo.
echo %ESC%[33mWebpack-dev-server hot reload is extremely slow when it used without %ESC%[1mcached volume%ESC%[0m %ESC%[33mdue to the size of the node_modules folder.%ESC%[0m
echo.
echo %ESC%[33mUnfortunately, Windows does not support %ESC%[1mcached volume%ESC%[0m %ESC%[33m...%ESC%[0m
echo %ESC%[33mPray for Windows improvements in the future ¯\_(ツ)_/¯%ESC%[0m
echo.
echo %ESC%[33mThere seems to be a workaround as explained in this tutorial: %ESC%[4mhttps://www.youtube.com/watch?v=dQw4w9WgXcQ%ESC%[0m
echo.
echo %ESC%[33mSee official Docker documentation: %ESC%[4mhttps://docs.docker.com/storage/bind-mounts/#configure-mount-consistency-for-macos%ESC%[0m
echo.
echo %ESC%[33mDue to this issue, the launching of the 'UI' container will be skipped.%ESC%[0m
echo.

choice /M "%ESC%[36mSkip 'UI' container launching? %ESC%[0m"
:: If choice = Y -> run only 'server' and 'database' services
IF ERRORLEVEL 1 (
  docker-compose --env-file %APP_DIR%\.docker\.env up -d database server
) ELSE (
  docker-compose --env-file %APP_DIR%\.docker\.env up -d
)
echo.

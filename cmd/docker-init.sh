#!/bin/bash

# --------------------------------------------------------------------------
# CMD: Set up project docker configuration
# @author Hervé Perchec <herve.perchec@gmail.com>
# --------------------------------------------------------------------------

# Get directory of 'app' parent folder and move into
SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd $SCRIPT_DIR/../
APP_DIR=$PWD

# Get environment variables
source $APP_DIR/.docker/.env.default

# Show header function
show_header() {
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo -e "\e[36mCMD: Set up project docker configuration\e[0m"
    echo -e "\e[36mAUTHOR: Hervé Perchec <herve.perchec@gmail.com>\e[0m"
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo
}

# kebab case function
kebab_case() {
    # Get first argument
    _str=$1
    # Replace space by -
    _str=${_str// /-}
    # Replace _ by -
    _str=${_str//_/-}
    # Convert to lowercase
    _str=${_str,,}
    # Return _str via echo command
    echo $_str
}

# --------------------------------------------------------------------------
# END INIT
# --------------------------------------------------------------------------

# Show header
show_header

# Copy .env.default to .env
cp $APP_DIR/.docker/.env.default $APP_DIR/.docker/.env

# --------------------------------------------------------------------------
# Step 1: Set COMPOSE_PROJECT_NAME
# --------------------------------------------------------------------------

read -e -p $'1 - \e[36mPROJECT NAME:\e[0m ' -i "$COMPOSE_PROJECT_NAME" COMPOSE_PROJECT_NAME
echo

# --------------------------------------------------------------------------
# Step 2: Set DB_IMAGE_NAME
# --------------------------------------------------------------------------

read -e -p $'2 - \e[36mDATABASE IMAGE NAME:\e[0m ' -i "$DB_IMAGE_NAME" DB_IMAGE_NAME
echo

# --------------------------------------------------------------------------
# Step 3: Set DB_CONTAINER_NAME
# --------------------------------------------------------------------------

read -e -p $'3 - \e[36mDATABASE CONTAINER NAME:\e[0m ' -i "$DB_CONTAINER_NAME" DB_CONTAINER_NAME
echo

# --------------------------------------------------------------------------
# Step 4: Set DB_MYSQL_CONTAINER_PORT
# --------------------------------------------------------------------------

read -e -p $'4 - \e[36mDATABASE CONTAINER PORT:\e[0m ' -i "$DB_MYSQL_CONTAINER_PORT" DB_MYSQL_CONTAINER_PORT
echo

# --------------------------------------------------------------------------
# Step 5: Set DB_MYSQL_LOCAL_PORT
# --------------------------------------------------------------------------

read -e -p $'5 - \e[36mDATABASE LOCAL PORT:\e[0m ' -i "$DB_MYSQL_LOCAL_PORT" DB_MYSQL_LOCAL_PORT
echo

# --------------------------------------------------------------------------
# Step 6: Set SERVER_IMAGE_NAME
# --------------------------------------------------------------------------

read -e -p $'6 - \e[36mSERVER IMAGE NAME:\e[0m ' -i "$SERVER_IMAGE_NAME" SERVER_IMAGE_NAME
echo

# --------------------------------------------------------------------------
# Step 7: Set SERVER_CONTAINER_NAME
# --------------------------------------------------------------------------

read -e -p $'7 - \e[36mSERVER CONTAINER NAME:\e[0m ' -i "$SERVER_CONTAINER_NAME" SERVER_CONTAINER_NAME
echo

# --------------------------------------------------------------------------
# Step 8: Set SERVER_LARAVEL_CONTAINER_PORT
# --------------------------------------------------------------------------

read -e -p $'8 - \e[36mSERVER CONTAINER PORT:\e[0m ' -i "$SERVER_LARAVEL_CONTAINER_PORT" SERVER_LARAVEL_CONTAINER_PORT
echo

# --------------------------------------------------------------------------
# Step 9: Set SERVER_LARAVEL_LOCAL_PORT
# --------------------------------------------------------------------------

read -e -p $'9 - \e[36mSERVER LOCAL PORT:\e[0m ' -i "$SERVER_LARAVEL_LOCAL_PORT" SERVER_LARAVEL_LOCAL_PORT
echo

# --------------------------------------------------------------------------
# Step 10: Set UI_IMAGE_NAME
# --------------------------------------------------------------------------

read -e -p $'10 - \e[36mUI IMAGE NAME:\e[0m ' -i "$UI_IMAGE_NAME" UI_IMAGE_NAME
echo

# --------------------------------------------------------------------------
# Step 11: Set UI_CONTAINER_NAME
# --------------------------------------------------------------------------

read -e -p $'11 - \e[36mUI CONTAINER NAME:\e[0m ' -i "$UI_CONTAINER_NAME" UI_CONTAINER_NAME
echo

# --------------------------------------------------------------------------
# Step 12: Set UI_NODE_CONTAINER_PORT
# --------------------------------------------------------------------------

read -e -p $'12 - \e[36mUI CONTAINER PORT:\e[0m ' -i "$UI_NODE_CONTAINER_PORT" UI_NODE_CONTAINER_PORT
echo

# --------------------------------------------------------------------------
# Step 13: Set UI_NODE_LOCAL_PORT
# --------------------------------------------------------------------------

read -e -p $'13 - \e[36mUI LOCAL PORT:\e[0m ' -i "$UI_NODE_LOCAL_PORT" UI_NODE_LOCAL_PORT
echo

# --------------------------------------------------------------------------
# Step 14: Set NETWORK_NAME
# --------------------------------------------------------------------------

NETWORK_NAME="$COMPOSE_PROJECT_NAME-network"
NETWORK_NAME=$(kebab_case "$NETWORK_NAME")

read -e -p $'14 - \e[36mNETWORK NAME:\e[0m ' -i "$NETWORK_NAME" NETWORK_NAME
echo

# --------------------------------------------------------------------------
# END OF PROMPTS
# --------------------------------------------------------------------------

# --------------------------------------------------------------------------
# Step 15: Update .docker/.env file
# --------------------------------------------------------------------------

sed -i 's/COMPOSE_PROJECT_NAME=.*/COMPOSE_PROJECT_NAME='"\"$COMPOSE_PROJECT_NAME\""'/' $APP_DIR/.docker/.env
sed -i 's/DB_IMAGE_NAME=.*/DB_IMAGE_NAME='"\"$DB_IMAGE_NAME\""'/' $APP_DIR/.docker/.env
sed -i 's/DB_CONTAINER_NAME=.*/DB_CONTAINER_NAME='"\"$DB_CONTAINER_NAME\""'/' $APP_DIR/.docker/.env
sed -i 's/DB_MYSQL_CONTAINER_PORT=.*/DB_MYSQL_CONTAINER_PORT='"$DB_MYSQL_CONTAINER_PORT"'/' $APP_DIR/.docker/.env
sed -i 's/DB_MYSQL_LOCAL_PORT=.*/DB_MYSQL_LOCAL_PORT='"$DB_MYSQL_LOCAL_PORT"'/' $APP_DIR/.docker/.env
sed -i 's/SERVER_IMAGE_NAME=.*/SERVER_IMAGE_NAME='"\"$SERVER_IMAGE_NAME\""'/' $APP_DIR/.docker/.env
sed -i 's/SERVER_CONTAINER_NAME=.*/SERVER_CONTAINER_NAME='"\"$SERVER_CONTAINER_NAME\""'/' $APP_DIR/.docker/.env
sed -i 's/SERVER_LARAVEL_CONTAINER_PORT=.*/SERVER_LARAVEL_CONTAINER_PORT='"$SERVER_LARAVEL_CONTAINER_PORT"'/' $APP_DIR/.docker/.env
sed -i 's/SERVER_LARAVEL_LOCAL_PORT=.*/SERVER_LARAVEL_LOCAL_PORT='"$SERVER_LARAVEL_LOCAL_PORT"'/' $APP_DIR/.docker/.env
sed -i 's/UI_IMAGE_NAME=.*/UI_IMAGE_NAME='"\"$UI_IMAGE_NAME\""'/' $APP_DIR/.docker/.env
sed -i 's/UI_CONTAINER_NAME=.*/UI_CONTAINER_NAME='"\"$UI_CONTAINER_NAME\""'/' $APP_DIR/.docker/.env
sed -i 's/UI_NODE_CONTAINER_PORT=.*/UI_NODE_CONTAINER_PORT='"$UI_NODE_CONTAINER_PORT"'/' $APP_DIR/.docker/.env
sed -i 's/UI_NODE_LOCAL_PORT=.*/UI_NODE_LOCAL_PORT='"$UI_NODE_LOCAL_PORT"'/' $APP_DIR/.docker/.env
sed -i 's/NETWORK_NAME=.*/NETWORK_NAME='"\"$NETWORK_NAME\""'/' $APP_DIR/.docker/.env

echo -e "\e[32mEnvironment variables for docker-compose successfully configured ✔\e[0m"
echo

echo -e "\e[36mCOMPOSE_PROJECT_NAME:\e[0m $COMPOSE_PROJECT_NAME"
echo -e "\e[36mDB_IMAGE_NAME:\e[0m $DB_IMAGE_NAME"
echo -e "\e[36mDB_CONTAINER_NAME:\e[0m $DB_CONTAINER_NAME"
echo -e "\e[36mDB_MYSQL_CONTAINER_PORT:\e[0m $DB_MYSQL_CONTAINER_PORT"
echo -e "\e[36mDB_MYSQL_LOCAL_PORT:\e[0m $DB_MYSQL_LOCAL_PORT"
echo -e "\e[36mSERVER_IMAGE_NAME:\e[0m $SERVER_IMAGE_NAME"
echo -e "\e[36mSERVER_CONTAINER_NAME:\e[0m $SERVER_CONTAINER_NAME"
echo -e "\e[36mSERVER_LARAVEL_CONTAINER_PORT:\e[0m $SERVER_LARAVEL_CONTAINER_PORT"
echo -e "\e[36mSERVER_LARAVEL_LOCAL_PORT:\e[0m $SERVER_LARAVEL_LOCAL_PORT"
echo -e "\e[36mUI_IMAGE_NAME:\e[0m $UI_IMAGE_NAME"
echo -e "\e[36mUI_CONTAINER_NAME:\e[0m $UI_CONTAINER_NAME"
echo -e "\e[36mUI_NODE_CONTAINER_PORT:\e[0m $UI_NODE_CONTAINER_PORT"
echo -e "\e[36mUI_NODE_LOCAL_PORT:\e[0m $UI_NODE_LOCAL_PORT"
echo -e "\e[36mNETWORK_NAME:\e[0m $NETWORK_NAME"
echo

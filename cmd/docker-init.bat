:: --------------------------------------------------------------------------
:: CMD: Set up project docker configuration
:: @author Hervé Perchec <herve.perchec@gmail.com>
:: --------------------------------------------------------------------------

:: Disable output excepted 'echo' messages and error messages
@echo off

:: Enable delayed expansion (for function in end of file)
setlocal enabledelayedexpansion

:: Enable UTF-8 output message (redirect output to NUL to not show message)
chcp 65001 > NUL

:: Enable color output
for /F %%a in ('echo prompt $E ^| cmd') do (
  set "ESC=%%a"
)

:: Empty line
echo.

:: Show script header
echo %ESC%[36m---------------------------------------------------------------%ESC%[0m
echo %ESC%[36mCMD: Set up project docker configuration%ESC%[0m
echo %ESC%[36mAUTHOR: Hervé Perchec ^<herve.perchec@gmail.com^>%ESC%[0m
echo %ESC%[36m---------------------------------------------------------------%ESC%[0m
echo.

:: Get directory of 'app' parent folder and move into
cd %~dp0..\
set APP_DIR=%cd%

:: Get environment variables
FOR /F "tokens=1* delims==" %%a in (%APP_DIR%\.docker\.env.default) do (
    set key=%%a
    set value=%%b
    :: If defined value, define variable and remove double quotes
    if defined value set "%%a=!value:"=!"
)

:: --------------------------------------------------------------------------
:: Step 1: Set COMPOSE_PROJECT_NAME
:: --------------------------------------------------------------------------

choice /M "1 - %ESC%[36mPROJECT NAME:%ESC%[0m %COMPOSE_PROJECT_NAME%"
:: If choice = N -> change project name
IF ERRORLEVEL 2 SET /p COMPOSE_PROJECT_NAME="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 2: Set DB_IMAGE_NAME
:: --------------------------------------------------------------------------

choice /M "2 - %ESC%[36mDATABASE IMAGE NAME:%ESC%[0m %DB_IMAGE_NAME%"
:: If choice = N -> change mysql image name
IF ERRORLEVEL 2 SET /p DB_IMAGE_NAME="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 3: Set DB_CONTAINER_NAME
:: --------------------------------------------------------------------------

choice /M "3 - %ESC%[36mDATABASE CONTAINER NAME:%ESC%[0m %DB_CONTAINER_NAME%"
:: If choice = N -> change mysql container name
IF ERRORLEVEL 2 SET /p DB_CONTAINER_NAME="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 4: Set DB_MYSQL_CONTAINER_PORT
:: --------------------------------------------------------------------------

choice /M "4 - %ESC%[36mDATABASE CONTAINER PORT:%ESC%[0m %DB_MYSQL_CONTAINER_PORT%"
:: If choice = N -> change mysql container port
IF ERRORLEVEL 2 SET /p DB_MYSQL_CONTAINER_PORT="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 5: Set DB_MYSQL_LOCAL_PORT
:: --------------------------------------------------------------------------

choice /M "5 - %ESC%[36mDATABASE LOCAL PORT:%ESC%[0m %DB_MYSQL_LOCAL_PORT%"
:: If choice = N -> change mysql local port
IF ERRORLEVEL 2 SET /p DB_MYSQL_LOCAL_PORT="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 6: Set SERVER_IMAGE_NAME
:: --------------------------------------------------------------------------

choice /M "6 - %ESC%[36mSERVER IMAGE NAME:%ESC%[0m %SERVER_IMAGE_NAME%"
:: If choice = N -> change php image name
IF ERRORLEVEL 2 SET /p SERVER_IMAGE_NAME="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 7: Set SERVER_CONTAINER_NAME
:: --------------------------------------------------------------------------

choice /M "7 - %ESC%[36mSERVER CONTAINER NAME:%ESC%[0m %SERVER_CONTAINER_NAME%"
:: If choice = N -> change php container name
IF ERRORLEVEL 2 SET /p SERVER_CONTAINER_NAME="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 8: Set SERVER_LARAVEL_CONTAINER_PORT
:: --------------------------------------------------------------------------

choice /M "8 - %ESC%[36mSERVER CONTAINER PORT:%ESC%[0m %SERVER_LARAVEL_CONTAINER_PORT%"
:: If choice = N -> change php container port
IF ERRORLEVEL 2 SET /p SERVER_LARAVEL_CONTAINER_PORT="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 9: Set SERVER_LARAVEL_LOCAL_PORT
:: --------------------------------------------------------------------------

choice /M "9 - %ESC%[36mSERVER LOCAL PORT:%ESC%[0m %SERVER_LARAVEL_LOCAL_PORT%"
:: If choice = N -> change php local port
IF ERRORLEVEL 2 SET /p SERVER_LARAVEL_LOCAL_PORT="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 10: Set UI_IMAGE_NAME
:: --------------------------------------------------------------------------

choice /M "10 - %ESC%[36mUI IMAGE NAME:%ESC%[0m %UI_IMAGE_NAME%"
:: If choice = N -> change node image name
IF ERRORLEVEL 2 SET /p UI_IMAGE_NAME="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 11: Set UI_CONTAINER_NAME
:: --------------------------------------------------------------------------

choice /M "11 - %ESC%[36mUI CONTAINER NAME:%ESC%[0m %UI_CONTAINER_NAME%"
:: If choice = N -> change node container name
IF ERRORLEVEL 2 SET /p UI_CONTAINER_NAME="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 12: Set UI_NODE_CONTAINER_PORT
:: --------------------------------------------------------------------------

choice /M "12 - %ESC%[36mUI CONTAINER PORT:%ESC%[0m %UI_NODE_CONTAINER_PORT%"
:: If choice = N -> change node container port
IF ERRORLEVEL 2 SET /p UI_NODE_CONTAINER_PORT="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 13: Set UI_NODE_LOCAL_PORT
:: --------------------------------------------------------------------------

choice /M "13 - %ESC%[36mUI LOCAL PORT:%ESC%[0m %UI_NODE_LOCAL_PORT%"
:: If choice = N -> change node local port
IF ERRORLEVEL 2 SET /p UI_NODE_LOCAL_PORT="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: Step 14: Set NETWORK_NAME
:: --------------------------------------------------------------------------

set NETWORK_NAME=%COMPOSE_PROJECT_NAME%-network

call :kebab_case NETWORK_NAME

choice /M "14 - %ESC%[36mNETWORK NAME:%ESC%[0m %NETWORK_NAME%"
:: If choice = N -> change network name
IF ERRORLEVEL 2 SET /p NETWORK_NAME="↳ Enter value: "
echo.

:: --------------------------------------------------------------------------
:: END OF PROMPTS
:: --------------------------------------------------------------------------

:: --------------------------------------------------------------------------
:: Step 15: Update .docker/.env file
:: --------------------------------------------------------------------------

:: Read each line of .env file (1 item by line, with delimiter: "=")
:: Redirect output to temporary .env.tmp file
(FOR /F "tokens=1* delims==" %%a in (%APP_DIR%\.docker\.env.default) do (
    @REM If the line is a comment -> %%b is empty
    @REM Else, a key-value pair is found and key is set in %%a, while value is set in %%b
    if "%%b"=="" (
        echo %%a
    ) else (
        if "%%a"=="COMPOSE_PROJECT_NAME" (echo COMPOSE_PROJECT_NAME="%COMPOSE_PROJECT_NAME%")
        if "%%a"=="DB_IMAGE_NAME" (echo DB_IMAGE_NAME="%DB_IMAGE_NAME%")
        if "%%a"=="DB_CONTAINER_NAME" (echo DB_CONTAINER_NAME="%DB_CONTAINER_NAME%")
        if "%%a"=="DB_MYSQL_CONTAINER_PORT" (echo DB_MYSQL_CONTAINER_PORT=%DB_MYSQL_CONTAINER_PORT%)
        if "%%a"=="DB_MYSQL_LOCAL_PORT" (echo DB_MYSQL_LOCAL_PORT=%DB_MYSQL_LOCAL_PORT%)
        if "%%a"=="DB_SSH_CONTAINER_PORT" (echo DB_SSH_CONTAINER_PORT=%DB_SSH_CONTAINER_PORT%)
        if "%%a"=="DB_SSH_LOCAL_PORT" (echo DB_SSH_LOCAL_PORT=%DB_SSH_LOCAL_PORT%)
        if "%%a"=="SERVER_IMAGE_NAME" (echo SERVER_IMAGE_NAME="%SERVER_IMAGE_NAME%")
        if "%%a"=="SERVER_CONTAINER_NAME" (echo SERVER_CONTAINER_NAME="%SERVER_CONTAINER_NAME%")
        if "%%a"=="SERVER_LARAVEL_CONTAINER_PORT" (echo SERVER_LARAVEL_CONTAINER_PORT=%SERVER_LARAVEL_CONTAINER_PORT%)
        if "%%a"=="SERVER_LARAVEL_LOCAL_PORT" (echo SERVER_LARAVEL_LOCAL_PORT=%SERVER_LARAVEL_LOCAL_PORT%)
        if "%%a"=="SERVER_SSH_CONTAINER_PORT" (echo SERVER_SSH_CONTAINER_PORT=%SERVER_SSH_CONTAINER_PORT%)
        if "%%a"=="SERVER_SSH_LOCAL_PORT" (echo SERVER_SSH_LOCAL_PORT=%SERVER_SSH_LOCAL_PORT%)
        if "%%a"=="UI_IMAGE_NAME" (echo UI_IMAGE_NAME="%UI_IMAGE_NAME%")
        if "%%a"=="UI_CONTAINER_NAME" (echo UI_CONTAINER_NAME="%UI_CONTAINER_NAME%")
        if "%%a"=="UI_NODE_CONTAINER_PORT" (echo UI_NODE_CONTAINER_PORT=%UI_NODE_CONTAINER_PORT%)
        if "%%a"=="UI_NODE_LOCAL_PORT" (echo UI_NODE_LOCAL_PORT=%UI_NODE_LOCAL_PORT%)
        if "%%a"=="UI_SSH_CONTAINER_PORT" (echo UI_SSH_CONTAINER_PORT=%UI_SSH_CONTAINER_PORT%)
        if "%%a"=="UI_SSH_LOCAL_PORT" (echo UI_SSH_LOCAL_PORT=%UI_SSH_LOCAL_PORT%)
        if "%%a"=="NETWORK_NAME" (echo NETWORK_NAME="%NETWORK_NAME%")
    )
))>%APP_DIR%\.docker\.env

echo %ESC%[32mEnvironment variables for docker-compose successfully configured ✔%ESC%[0m
echo.

echo File %ESC%[33m%APP_DIR%\.docker\.env%ESC%[0m updated.
echo.

echo %ESC%[36mCOMPOSE_PROJECT_NAME:%ESC%[0m %COMPOSE_PROJECT_NAME%
echo %ESC%[36mDB_IMAGE_NAME:%ESC%[0m %DB_IMAGE_NAME%
echo %ESC%[36mDB_CONTAINER_NAME:%ESC%[0m %DB_CONTAINER_NAME%
echo %ESC%[36mDB_MYSQL_CONTAINER_PORT:%ESC%[0m %DB_MYSQL_CONTAINER_PORT%
echo %ESC%[36mDB_MYSQL_LOCAL_PORT:%ESC%[0m %DB_MYSQL_LOCAL_PORT%
echo %ESC%[36mSERVER_IMAGE_NAME:%ESC%[0m %SERVER_IMAGE_NAME%
echo %ESC%[36mSERVER_CONTAINER_NAME:%ESC%[0m %SERVER_CONTAINER_NAME%
echo %ESC%[36mSERVER_LARAVEL_CONTAINER_PORT:%ESC%[0m %SERVER_LARAVEL_CONTAINER_PORT%
echo %ESC%[36mSERVER_LARAVEL_LOCAL_PORT:%ESC%[0m %SERVER_LARAVEL_LOCAL_PORT%
echo %ESC%[36mUI_IMAGE_NAME:%ESC%[0m %UI_IMAGE_NAME%
echo %ESC%[36mUI_CONTAINER_NAME:%ESC%[0m %UI_CONTAINER_NAME%
echo %ESC%[36mUI_NODE_CONTAINER_PORT:%ESC%[0m %UI_NODE_CONTAINER_PORT%
echo %ESC%[36mUI_NODE_LOCAL_PORT:%ESC%[0m %UI_NODE_LOCAL_PORT%
echo %ESC%[36mNETWORK_NAME:%ESC%[0m %NETWORK_NAME%
echo.

exit 0

:: --------------------------------------------------------------------------
:: FUNCTIONS DECLARATIONS
:: --------------------------------------------------------------------------

@REM Function to replace uppercase to lowercase
:kebab_case
:: Get first parameter (string to convert)
set _str=!%~1!
:: Declare characters map
set "_TO_REPLACE=ABCDEFGHIJKLMNOPQRSTUVWXYZ_"
set "_REPLACE_BY=abcdefghijklmnopqrstuvwxyz-"
:: Convert string
for /l %%a in (0,1,25) do (
    call set "_FROM=%%_TO_REPLACE:~%%a,1%%"
    call set "_TO=%%_REPLACE_BY:~%%a,1%%"
    call set "_str=%%_str:!_FROM!=!_TO!%%"
)
:: Replaces spaces by -
set _str=%_str: =-%
:: Apply to passed argument
set "%~1=%_str%"
GOTO:EOF